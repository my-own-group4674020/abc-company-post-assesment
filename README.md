This is a Spring Boot project , fully automated CI/CD application for ABC company.
the port is 9090.
process/software used
1. project specific gitlab runner which is hosted in Ubuntu for Windows in my local system
2. tag is been used as "ubuntu"
3. Docker is been used to create image.
4. installed docker desktop in local machine
5. Gitlab container registry is been used for image push registry.
6. upon running application after successfully deployment. the message in GUI : Md Abid Hossain. Welcome to Abc Company CI/CD application!
7. end point is http://localhost:9090/