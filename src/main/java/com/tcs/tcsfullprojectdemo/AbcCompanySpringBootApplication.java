package com.tcs.tcsfullprojectdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class AbcCompanySpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(AbcCompanySpringBootApplication.class, args);
	}

	@GetMapping("/")
    public String hello(@RequestParam(value = "name", defaultValue = "Md Abid Hossain. Welcome to Abc Company CI/CD application") String name) {
      return String.format("Hello %s!", name);
    }
}
